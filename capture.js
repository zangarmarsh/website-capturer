const puppeteer = require('puppeteer');

if (!process.argv[2]) {
  const fileName = process.argv[1].split('/').pop();
  console.log(`Usage: node ${fileName} [url] [?output_path] [?width] [?height]`);
  return;
}

(async () => {
const browser = await puppeteer.launch();

try {
  const page = await browser.newPage();
  await page.setViewport({
    width: parseInt(process.argv[4]) || 1920,
    height: parseInt(process.argv[5]) || 1080,
    deviceScaleFactor: 1,
  });

  await page.setDefaultNavigationTimeout(40 * 1000); 
  await page.goto(process.argv[2], { waitUntil: 'networkidle0'});
  await page.screenshot({ path: process.argv[3] || 'screenshot.png' });
} catch (e) {
    console.log(e);
} finally {
    if (browser) {
      await browser.close();
    }
}
})();
